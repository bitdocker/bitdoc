global class CreateSalesforceAccountInbound implements Messaging.InboundEmailHandler {

    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envilope)
    {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String accName = email.fromName;
        String accDesc = email.plainTextBody;
        String industry = email.subject;
        Account ac = new Account(Name = accName, description = accDesc, Industry = industry);
        insert ac;
        return null;
    }
}